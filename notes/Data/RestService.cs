﻿//#define OFFLINE_SYNC_ENABLED
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.MobileServices;
using System.Collections.ObjectModel;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using SQLitePCL.Extensions;


namespace notes
{
    public class RestService : IRestService
    {
        //HttpClient client;
        //IMobileServiceTable<note> tblnote;
        //MobileServiceClient client;

        IMobileServiceSyncTable<note> tblnote;


        public List<note> Items { get; private set; }

        public static MobileServiceClient client;
        HttpClient httpclient;
        public static MobileServiceSQLiteStore dbstore;
        public RestService()
        {
            //			var authData = string.Format ("{0}:{1}", Constants.Username, Constants.Password);
            //			var authHeaderValue = Convert.ToBase64String (Encoding.UTF8.GetBytes (authData));
            //
            //			client = new HttpClient ();
            //			client.MaxResponseContentBufferSize = 256000;
            //			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Basic", authHeaderValue);

            httpclient = new HttpClient();
            httpclient.MaxResponseContentBufferSize = 256000;

        }
        public async Task Initialize()
        {
            client = new MobileServiceClient(
               Constants.RestUrl

           );

            dbstore = new MobileServiceSQLiteStore("localstore94.db1");
            dbstore.DefineTable<note>();

            try
            {
                if (!client.SyncContext.IsInitialized)
                    Task.Run(() => client.SyncContext.InitializeAsync(dbstore, new MobileServiceSyncHandler())).Wait();
            }
            catch (MobileServicePushFailedException exc)
            {
                //if (exc.PushResult != null)
                //{
                //    syncErrors = exc.PushResult.Errors;
                //}
            }
            catch (MobileServiceInvalidOperationException iexc)
            {
                Debug.WriteLine(iexc.Message);
            }

            tblnote = client.GetSyncTable<note>();
        }
        public async Task<ObservableCollection<note>> RefreshDataAsync(bool syncItems = false)
        {
            try
            {

                if (syncItems)
                {
                    await SyncAsync();
                }
                IEnumerable<note> items = await tblnote
                    .Where(todoItem => !todoItem.isDone)
                    .ToEnumerableAsync();

                return new ObservableCollection<note>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }
        //		public async Task<List<note>> RefreshDataAsync ()
        //		{
        //			try
        //			{
        //				Items = await tblnote.ToListAsync();
        //				return Items;
        //			}
        //			catch (MobileServiceInvalidOperationException msioe)
        //			{
        //				Debug.WriteLine(@"INVALID {0}", msioe.Message);
        //			}
        //			catch (Exception e)
        //			{
        //				Debug.WriteLine(@"ERROR {0}", e.Message);
        //			}
        //			return null;
        //		}
        public async Task SaveNoteItemAsyncRest(note item, bool isNewItem = false)
        {
            // RestUrl = http://developer.xamarin.com:8081/api/todoitems{0}
            item.ID = Guid.NewGuid().ToString();
            var uri = new Uri(string.Format(Constants.RestAzureUrl, item.ID));

            try
            {
                var json = JsonConvert.SerializeObject(item);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                if (isNewItem)
                {
                    response = await httpclient.PostAsync(uri, content);
                }
                else
                {
                    response = await httpclient.PutAsync(uri, content);
                }

                if (response.IsSuccessStatusCode)
                {
                    Debug.WriteLine(@"             Note successfully saved.");
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"             ERROR {0}", ex.Message);
            }
        }
        public async Task SavenoteAsync(note item, bool isNewItem = false)
        {

            try
            {
                if (isNewItem)
                {
                    item.ID = Guid.NewGuid().ToString();
                    await tblnote.InsertAsync(item);
                }
                else
                {
                    await tblnote.UpdateAsync(item);
                }

                //await client.SyncContext.PushAsync();
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"INVALID {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }
        }

        public async Task DeletenoteAsync(note item)
        {
            try
            {
                await tblnote.DeleteAsync(item);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"INVALID {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"ERROR {0}", e.Message);
            }
        }
        public async Task SyncAsync()
        {
            ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

            try
            {

                await client.SyncContext.PushAsync();
                await tblnote.PullAsync(
                //The first parameter is a query name that is used internally by the client SDK to implement incremental sync.
                //Use a different query name for each unique query in your program
                null,
                tblnote.CreateQuery());

            }
            catch (MobileServicePushFailedException exc)
            {
                if (exc.PushResult != null)
                {
                    syncErrors = exc.PushResult.Errors;
                }
            }

            // Simple error/conflict handling. A real application would handle the various errors like network conditions,
            // server conflicts and others via the IMobileServiceSyncHandler.
            if (syncErrors != null)
            {
                foreach (var error in syncErrors)
                {
                    if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
                    {
                        //Update failed, reverting to server's copy.
                        await error.CancelAndUpdateItemAsync(error.Result);
                    }
                    else
                    {
                        // Discard local change.
                        await error.CancelAndDiscardItemAsync();
                    }

                    Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
                }
            }
        }
    }
}

