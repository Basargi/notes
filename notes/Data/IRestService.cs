﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace notes
{
    public interface IRestService
    {
        Task<ObservableCollection<note>> RefreshDataAsync(bool syncItems = false);

        Task SavenoteAsync(note item, bool isNewItem);
        Task SaveNoteItemAsyncRest(note item, bool isNewItem);

        Task DeletenoteAsync(note item);
    }
}

