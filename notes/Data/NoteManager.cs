﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace notes
{
    public class NoteManager
    {
        IRestService restService;

        public NoteManager(IRestService service)
        {
            restService = service;
        }

        public Task<ObservableCollection<note>> GetTasksAsync(bool isSync)
        {
            return restService.RefreshDataAsync(isSync);
        }

        public Task SaveTaskAsync(note item, bool isNewItem = false)
        {
            return restService.SavenoteAsync(item, isNewItem);
        }
        public Task SaveRestTaskAsync(note item, bool isNewItem = false)
        {
            return restService.SaveNoteItemAsyncRest(item, isNewItem);
        }
        public Task DeleteTaskAsync(note item)
        {
            return restService.DeletenoteAsync(item);
        }
    }
}

