﻿using System;

using Xamarin.Forms;

namespace notes
{
	public class App : Application
	{
		public static NoteManager noteManager { get; private set; }
public static RestService _RestService { get; private set; }
	

		public App ()
		{
            MainPage=new NavigationPage(new ContentPage { 
                Title=""
            });
		}


		protected override async void OnStart ()
		{
            _RestService = new RestService();
            await _RestService.Initialize();
			noteManager = new NoteManager(_RestService);
            MainPage = new NavigationPage(new ViewNotes ());
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

