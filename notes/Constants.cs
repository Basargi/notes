﻿using System;

namespace notes
{
    public static class Constants
    {
        // URL of REST service
        //public static string RestUrl = "http://developer.xamarin.com:8081/api/todoitems{0}";
        // Credentials that are hard coded into the REST service
        //public static string Username = "Xamarin";
        //public static string Password = "Pa$$w0rd";

        public static string RestUrl = @"https://mcdhs.azurewebsites.net";
        public static string RestAzureUrl = @"https://mcdhs.azurewebsites.net/api/note{0}";
    }
}

