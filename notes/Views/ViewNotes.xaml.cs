﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace notes
{
	public partial class ViewNotes : ContentPage
	{
		bool alertShown = false;

		public ViewNotes ()
		{
			InitializeComponent ();
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();
			listView.ItemsSource = await App.noteManager.GetTasksAsync (true);
		}

		void OnAddItemClicked (object sender, EventArgs e)
		{
			var noteItem = new note ();
			var addPage = new AddNote (true);
			addPage.BindingContext = noteItem;
			Navigation.PushAsync (addPage);
		}

		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var noteItem = e.SelectedItem as note;
			var addPage = new AddNote ();
			addPage.BindingContext = noteItem;
			Navigation.PushAsync (addPage);
		}
	}
}

