﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace notes
{
    public partial class AddNote : ContentPage
    {
        bool isNewItem;

        public AddNote(bool isNew = false)
        {
            InitializeComponent();
            isNewItem = isNew;
        }

        async void OnSaveActivated(object sender, EventArgs e)
        {
            var todoItem = (note)BindingContext;
            await App.noteManager.SaveTaskAsync(todoItem, isNewItem);
            await Navigation.PopAsync();
        }
        async void OnRestSaveActivated(object sender, EventArgs e)
        {
            var todoItem = (note)BindingContext;
            await App.noteManager.SaveTaskAsync(todoItem, isNewItem);
            await Navigation.PopAsync();
        }
        async void OnDeleteActivated(object sender, EventArgs e)
        {
            var todoItem = (note)BindingContext;
            await App.noteManager.DeleteTaskAsync(todoItem);
            await Navigation.PopAsync();
        }

        void OnCancelActivated(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}

