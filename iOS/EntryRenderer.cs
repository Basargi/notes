﻿using System;
using notes;
using notes.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(ModifiedEntry), typeof(MyEntryRenderer))]
namespace notes.iOS
{
public class MyEntryRenderer : EntryRenderer
{
	protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
	{
		base.OnElementChanged(e);

		if (e.OldElement == null)
		{
			// do whatever you want to the UITextField here!
			Control.BackgroundColor = UIColor.Black;
                Control.TextColor = UIColor.White;
			Control.BorderStyle = UITextBorderStyle.Bezel;
		}
 }
    }
}
